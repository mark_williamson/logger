package org.ohmslaw.logs;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class LogAnalyserCommandLineParserTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    private LogAnalyserCommandLineParser parser;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));

    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void testThatHelpIsDisplayed() {
        String[] args = {"--help"};
        parser = new LogAnalyserCommandLineParser();
        String expected = "Unexpected exception:Missing required option: f\n" +
                "usage: log analyser\n" +
                " -f,--file <arg>   the name of the file to parse\n" +
                " -h,--help         this help text\n";

        parser.parser(args);
        assertThat(outContent.toString(), is(expected));
    }

}
