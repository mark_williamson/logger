package org.ohmslaw.logs;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class UrlVistitorTest {

    @Test
    public void testThatTop3IPSAreReturnedInAlphaOrder() {
        URLVisitor uRLVisitor = new URLVisitor();

        uRLVisitor.visit("z");
        uRLVisitor.visit("x");
        uRLVisitor.visit("y");
        uRLVisitor.visit("a");
        uRLVisitor.visit("b");
        uRLVisitor.visit("c");
        uRLVisitor.visit("d");
        uRLVisitor.visit("e");

        assertThat(uRLVisitor.topThreeURLS().size(),is(3));
        assertThat(uRLVisitor.topThreeURLS().get(0),is("a"));
        assertThat(uRLVisitor.topThreeURLS().get(1),is("b"));
        assertThat(uRLVisitor.topThreeURLS().get(2),is("c"));
    }

    @Test
    public void testThatTop3IPSAreReturned() {
        URLVisitor uRLVisitor = new URLVisitor();

        uRLVisitor.visit("z");
        uRLVisitor.visit("x");
        uRLVisitor.visit("y");
        uRLVisitor.visit("a");
        uRLVisitor.visit("b");
        uRLVisitor.visit("b");
        uRLVisitor.visit("b");
        uRLVisitor.visit("c");
        uRLVisitor.visit("d");
        uRLVisitor.visit("e");
        uRLVisitor.visit("e");


        assertThat(uRLVisitor.topThreeURLS().size(),is(3));
        assertThat(uRLVisitor.topThreeURLS().get(0),is("b"));
        assertThat(uRLVisitor.topThreeURLS().get(1),is("e"));
        assertThat(uRLVisitor.topThreeURLS().get(2),is("a"));
    }
}
