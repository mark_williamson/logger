package org.ohmslaw.logs;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ProcessLogTest {

    @Mock
    private UniqueIPCounter mockIPCounter;

    @Mock
    private URLVisitor mockUrlVisitor;

    @Before
    public void setup() {
        mockIPCounter = Mockito.mock(UniqueIPCounter.class);
        mockUrlVisitor = Mockito.mock(URLVisitor.class);
    }

    @Test
    public void testSimpleProcessLog() throws IOException {
        ProcessLog processLog = new ProcessLog();

        processLog.process(testFile(),mockIPCounter,mockUrlVisitor);
        verify(mockIPCounter, times(2)).count(anyString());
        verify(mockUrlVisitor, times(2)).visit(anyString());
    }


    private File testFile() {
        File temp = null;
        try {
            // Create temp file.
             temp = File.createTempFile("test", ".log");

            // Delete temp file when program exits.
            temp.deleteOnExit();

            // Write to temp file
            BufferedWriter out = new BufferedWriter(new FileWriter(temp));
            out.write("72.44.32.10 - - [09/Jul/2018:15:48:20 +0200] \"GET /download/counter/ HTTP/1.1\" 200 3574 \"-\" \"Mozilla/5.0 (X11; U; Linux x86; en-US) AppleWebKit/534.7 (KHTML, like Gecko) Epiphany/2.30.6 Safari/534.7\"\n");
            out.write("50.112.00.11 - admin [11/Jul/2018:17:33:01 +0200] \"GET /asset.css HTTP/1.1\" 200 3574 \"-\" \"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6\"\n");

            out.close();

        } catch (IOException e) {
        }
        return temp;
    }
}
