package org.ohmslaw.logs;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

public class UniqueIPCounterTest {

    @Test
    public void testThatNoCountsReturnZero() {
        UniqueIPCounter uniqueIPCounter = new UniqueIPCounter();

        assertThat(uniqueIPCounter.getUniqueCount(),is(0));
    }

    @Test
    public void testThatIPsAreDeduped() {
        UniqueIPCounter uniqueIPCounter = new UniqueIPCounter();

        uniqueIPCounter.count("192.168.0.1");
        uniqueIPCounter.count("192.168.0.1");
        uniqueIPCounter.count("192.168.0.2");
        assertThat(uniqueIPCounter.getUniqueCount(),is(2));
    }

    @Test
    public void testThatTop3IPSAreReturned() {
        UniqueIPCounter uniqueIPCounter = new UniqueIPCounter();

        uniqueIPCounter.count("192.168.0.1");
        uniqueIPCounter.count("192.168.0.1");
        uniqueIPCounter.count("192.168.0.1");
        uniqueIPCounter.count("192.168.0.2");
        uniqueIPCounter.count("192.168.0.2");
        uniqueIPCounter.count("192.168.0.3");
        uniqueIPCounter.count("192.168.0.3");
        uniqueIPCounter.count("192.168.0.4");
        uniqueIPCounter.count("192.168.0.5");
        uniqueIPCounter.count("192.168.0.6");
        uniqueIPCounter.count("192.168.0.6");
        uniqueIPCounter.count("192.168.0.6");
        uniqueIPCounter.count("192.168.0.6");
        uniqueIPCounter.count("192.168.0.6");
        uniqueIPCounter.count("192.168.0.6");

        assertThat(uniqueIPCounter.topThreeIps().size(),is(3));
        assertThat(uniqueIPCounter.topThreeIps().get(0),is("192.168.0.6"));
        assertThat(uniqueIPCounter.topThreeIps().get(1),is("192.168.0.1"));
        assertThat(uniqueIPCounter.topThreeIps().get(2),is("192.168.0.2"));
    }
}
