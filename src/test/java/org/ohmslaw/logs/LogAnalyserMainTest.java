package org.ohmslaw.logs;

import org.apache.commons.cli.CommandLine;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class LogAnalyserMainTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @Mock
    private UniqueIPCounter uniquieIPCounterMock;

    @Mock
    private URLVisitor urlVisitorMock;

    @Mock
    private ProcessLog processLogMock;

    @Mock
    private LogAnalyserCommandLineParser commandLineParserMock;

    @Mock
    private CommandLine commandLineMock;

    private LogAnalyzerMain logAnalyzerMain;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));

        processLogMock = Mockito.mock(ProcessLog.class);
        uniquieIPCounterMock = Mockito.mock(UniqueIPCounter.class);
        urlVisitorMock = Mockito.mock(URLVisitor.class);
        commandLineParserMock = Mockito.mock(LogAnalyserCommandLineParser.class);
        commandLineMock =   Mockito.mock(CommandLine.class);
        logAnalyzerMain = new LogAnalyzerMain(processLogMock,uniquieIPCounterMock,urlVisitorMock,commandLineParserMock);

        when(commandLineParserMock.parser(any())).thenReturn(commandLineMock);


    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void thatAskingForHelpDisplaysAHelpMessage() throws Exception {
        String[] args = {"--help"};
        when(commandLineMock.hasOption("help")).thenReturn(true);
        String expected = "Log analyser\n\n";

        logAnalyzerMain.run(args);
        assertThat(outContent.toString(), is(expected));
    }

    @Test
    public void testMessagesForANormalRun() throws Exception  {
        String[] args = {"--file atest"};
        when(commandLineMock.getOptionValue("file")).thenReturn("../../resources/data/sampleLogFile.log");
        when(uniquieIPCounterMock.topThreeIps()).thenReturn(getIPList());

        when(uniquieIPCounterMock.getUniqueCount()).thenReturn(12);
        when(urlVisitorMock.topThreeURLS()).thenReturn(getURLList());

        String expected = "Log analyser\n\nThere are 12 unique IPs\n" +
                "The 3 most active IPs are \n" +
                "     IP-A\n" +
                "     ip-B\n" +
                "     ip-C\n" +
                "The 3 most visited URLs are \n" +
                "     URL-A\n" +
                "     URL-B\n" +
                "     URL-C\n";

        logAnalyzerMain.run(args);
        assertThat(outContent.toString(), is(expected));

    }

    private List<String> getURLList() {
        return Arrays.asList("URL-A","URL-B","URL-C");
    }

    private List<String> getIPList() {
        return Arrays.asList("IP-A","ip-B","ip-C");
    }
}
