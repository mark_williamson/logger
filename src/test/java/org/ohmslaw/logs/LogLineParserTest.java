package org.ohmslaw.logs;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class LogLineParserTest {


    @Test
    public void testThatTheIPAddressIsParsed() {
        LogLineParser logLineParser = LogLineParser.from("177.71.128.21 - - [10/Jul/2018:22:21:28 +0200] \"GET /intranet-analytics/ HTTP/1.1\" 200 3574 \"-\" \"Mozilla/5.0 (X11; U; Linux x86_64; fr-FR) AppleWebKit/534.7 (KHTML, like Gecko) Epiphany/2.30.6 Safari/534.7\"");

        assertThat(logLineParser.getIP(),is("177.71.128.21"));
        assertThat(logLineParser.getURL(),is("/intranet-analytics/"));
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testForAnInvalidLogLine() {
        expectedEx.expect(RuntimeException.class);
        expectedEx.expectMessage("The logger text is not validly formed");
        LogLineParser logLineParser = LogLineParser.from("Not Valid");
    }
}
