package org.ohmslaw.logs;

import org.apache.commons.cli.CommandLine;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.File;


@Component
public class LogAnalyzerMain implements CommandLineRunner {

    private final ProcessLog processLog;

    private final UniqueIPCounter uniqueIPCounter;

    private final URLVisitor uRLVisitor;

    private final LogAnalyserCommandLineParser commandLineParser;

    public LogAnalyzerMain(ProcessLog processLog, UniqueIPCounter uniqueIPCounter, URLVisitor uRLVisitor, LogAnalyserCommandLineParser commandLineParser) {
        this.processLog = processLog;
        this.uniqueIPCounter = uniqueIPCounter;
        this.uRLVisitor = uRLVisitor;
        this.commandLineParser = commandLineParser;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Log analyser");
        System.out.println("");
        CommandLine commandLine = commandLineParser.parser(args);
        if (commandLine == null ||  commandLine.hasOption("help")) {
            return;
        }

        File logFile = new File(commandLine.getOptionValue("file"));
        processLog.process(logFile,uniqueIPCounter,uRLVisitor);

        System.out.println("There are " + uniqueIPCounter.getUniqueCount() + " unique IPs");
        System.out.println("The 3 most active IPs are ");
        uniqueIPCounter.topThreeIps().forEach(ip -> System.out.println("     " + ip));
        System.out.println("The 3 most visited URLs are ");
        uRLVisitor.topThreeURLS().forEach(ip -> System.out.println("     " + ip));
    }


}
