package org.ohmslaw.logs;

import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class UniqueIPCounter {

    private final Map<String,Integer> ipMap = new HashMap();;

    public void count(String ipAddress) {
        ipMap.putIfAbsent(ipAddress,0);
        ipMap.put(ipAddress,ipMap.get(ipAddress) +1);
    }


    public Integer getUniqueCount() {
        return ipMap.keySet().size();
    }

    public List topThreeIps() {
        return ipMap.entrySet().stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .map(Map.Entry::getKey)
                .limit(3)
                .collect(Collectors.toList());
    }
}
