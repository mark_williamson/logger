package org.ohmslaw.logs;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogLineParser {

    private final String logEntryPattern = "^(\\S+) (\\S+) (\\S+) \\[([\\w:/]+\\s[+\\-]\\d{4})\\] \"(\\S+) (\\S+)\\s*(\\S+)?\\s*\" (\\d{3}) (\\S+)";
    private final Pattern p = Pattern.compile(logEntryPattern);
    private  Matcher matcher;

    private LogLineParser() {}

    public static LogLineParser from(String line) {
        LogLineParser logLineParser = new LogLineParser();
        logLineParser.parse(line);
        return logLineParser;
    }

    private void parse(String line) {
        matcher = p.matcher(line);
        if (! matcher.find()) {
            throw new RuntimeException("The logger text is not validly formed");
        }
    }


    public String getIP() {
        return matcher.group(1);
    }

    public String getURL() {
        return matcher.group(6);
    }




}
