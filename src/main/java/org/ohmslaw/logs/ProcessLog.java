package org.ohmslaw.logs;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
public class ProcessLog {


    public void process(File logFile, UniqueIPCounter uniqueIPCounter, URLVisitor uRLVisitor) throws IOException {


        LineIterator it = FileUtils.lineIterator(logFile, "UTF-8");
        try {
            while (it.hasNext()) {
                String line = it.nextLine();

                LogLineParser parser = LogLineParser.from(line);
                uniqueIPCounter.count(parser.getIP());
                uRLVisitor.visit(parser.getURL());
            }
        } finally {
            LineIterator.closeQuietly(it);
        }
    }
}
