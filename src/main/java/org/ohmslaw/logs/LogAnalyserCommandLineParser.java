package org.ohmslaw.logs;

import org.apache.commons.cli.*;
import org.springframework.stereotype.Component;

@Component
public class LogAnalyserCommandLineParser {

    public CommandLine parser( String[] args) {
        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine = null;
        Options options = new Options();
        options.addOption(Option.builder("f").required().desc("the name of the file to parse").hasArg().longOpt("file").build());
        options.addOption("h", "help", false, "this help text");


        try {
            commandLine = parser.parse(options, args);
            if (commandLine.hasOption("help")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp( "log analyser", options );
            }

        } catch (ParseException exp) {
            System.out.println("Unexpected exception:" + exp.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "log analyser", options );
        }
        return commandLine;
    }
}
