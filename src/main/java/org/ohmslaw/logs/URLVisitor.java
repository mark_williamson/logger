package org.ohmslaw.logs;

import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class URLVisitor {

    private final Map<String,Integer> urlMap = new HashMap();

    public void visit(String url) {
        urlMap.putIfAbsent(url,0);
        urlMap.put(url, urlMap.get(url) +1);
    }

    public List topThreeURLS() {
        return urlMap.entrySet().stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .map(Map.Entry::getKey)
                .limit(3)
                .collect(Collectors.toList());

    }
}
