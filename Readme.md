# Log Analyser
A tool which implements a simple SpringBoot application that allows for reporting of log statistics

## Author: Mark Williamson

### Requirements

* Java 8 or better
* Gradle 5.x or better

### Building
``
gradle build 
``

This builds a spring boot application containing all dependencies

### Running

``
 java -jar  build/libs/logs-1.0.0.jar --file <File>
``
 
*For Example*

 ``
 java -jar  build/libs/logs-1.0.0.jar --file src/test/resources/data/sampleLogFile.log 
``
 
*To get help*
 
``
 java -jar  build/libs/logs-1.0.0.jar  --help
``


